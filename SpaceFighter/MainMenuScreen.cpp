
#include <string>
#include "MainMenuScreen.h"
#include "GameplayScreen.h"

// Callback Functions
void OnStartGameSelect(MenuScreen *pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen());
}

void OnQuitSelect(MenuScreen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	pMainMenuScreen->SetQuitFlag();
	pMainMenuScreen->Exit();
}

void OnScreenRemove(Screen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	if (pMainMenuScreen->IsQuittingGame()) pScreen->GetGame()->Quit();
}



MainMenuScreen::MainMenuScreen()
{
	m_pTexture = nullptr;

	SetRemoveCallback(OnScreenRemove);

	SetTransitionInTime(2.0f); //menu load transition
	SetTransitionOutTime(0.5f);

	Show(); // Show the screen
}

void MainMenuScreen::LoadContent(ResourceManager *pResourceManager)
{
	// Loads in the logo textures from the file
	m_pTexture = pResourceManager->Load<Texture>("Textures\\Logo.png"); 
	//gets the size of the screen
	m_texturePosition = Game::GetScreenCenter() - Vector2::UNIT_Y * 150;

	// Create the menu items
	const int COUNT = 2; //count of menu items
	MenuItem *pItem; //creation of pointer
	Font::SetLoadSize(20, true); //Size of the font when loaded in
	Font *pFont = pResourceManager->Load<Font>("Fonts\\Ethnocentric.ttf"); //sets the font from file //Changed font on text

	SetDisplayCount(COUNT); //displays count

	enum Items { START_GAME, QUIT }; //enumeration of buttons on menu
	std::string text[COUNT] = { "Start Game", "Quit" };  //titles buttons in count

	for (int i = 0; i < COUNT; i++) //for loop
	{
		pItem = new MenuItem(text[i]); //creates menu
		pItem->SetPosition(Vector2(100, 100 + 50 * i)); //actually sets screen position
		pItem->SetFont(pFont);  //actually sets font
		pItem->SetColor(Color::RebeccaPurple); //sets the color of the first menu item //Changed this color
		pItem->SetSelected(i == 0); //selects first
		AddMenuItem(pItem); //adds menu item fully
	}

	GetMenuItem(START_GAME)->SetSelectCallback(OnStartGameSelect); //enums start button
	GetMenuItem(QUIT)->SetSelectCallback(OnQuitSelect); //enums quit button
}

void MainMenuScreen::Update(const GameTime *pGameTime)
{
	MenuItem *pItem;

	// Set the menu item colors
	for (int i = 0; i < GetDisplayCount(); i++)
	{
		pItem = GetMenuItem(i);
		pItem->SetAlpha(GetAlpha());

		if (pItem->IsSelected()) pItem->SetColor(Color::Red); //Text color for  selected
		else pItem->SetColor(Color::RebeccaPurple); //text color for non selected
	}

	MenuScreen::Update(pGameTime);
}

void MainMenuScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();
	pSpriteBatch->Draw(m_pTexture, m_texturePosition, Color::White * GetAlpha(), m_pTexture->GetCenter());
	pSpriteBatch->End();

	MenuScreen::Draw(pSpriteBatch);
}
